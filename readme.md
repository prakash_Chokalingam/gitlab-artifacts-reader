# Gitlab Artifacts Reader
Simple client level gitlab pipeline artifacts reader

[https://gitlab-artifcats-reader.netlify.app](https://gitlab-artifcats-reader.netlify.app)


![sample](https://gitlab-artifcats-reader.netlify.app/sample.png)

## Stacks used:

[vitejs](https://vitejs.dev/)
[Vuejs](https://vuejs.org/) 
[Tailwind](https://tailwindcss.com/) 

## Deployed on:

[Netlify](https://netlify.com)


## Steps to run this project

* Install dependences `npm install`

* Start the dev server: `npm run dev`



